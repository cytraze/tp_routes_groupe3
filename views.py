from flask import render_template, flash, redirect
from app import app

@app.route('/', methods=('GET',) )
def index():
    return render_template("index.html",
                           title = "Home",
                          )

@app.route('/truc', methods=('GET',) )
def perdu():
    return "Tu t'es perdu", 404

@app.route('/meslistes', methods=('GET',) )
def route_meslistes():
    return "Page non implémentée", 501

@app.route('/meslistes/modifliste', methods=('GET',) )
def route_modifliste():
    return "Page non implémentée", 501

@app.route('/meslistes/modifliste/ajoutproduit', methods=('GET',) )
def route_ajoutproduit():
    return "Page non implémentée", 501

@app.route('/meslistes/modifliste/calculprixenseigne', methods=('GET',) )
def route_calculprixenseigne():
    return "Page non implémentée", 501

@app.route('/meslistes/modifliste/calculprixmax', methods=('GET',) )
def route_calculprixmax():
    return "Page non implémentée", 501

@app.route('/mescategories', methods=('GET',) )
def route_mescategories():
    return "Page non implémentée", 501

@app.route('/mesmagasins', methods=('GET',) )
def route_mesmagasins():
    return "Page non implémentée", 501

@app.route('/mesrecettes', methods=('GET',) )
def route_mesrecettes():
    return "Page non implémentée", 501
